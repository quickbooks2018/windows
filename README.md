# gitlab runner for windows


- official doc url

- https://docs.gitlab.com/runner/install/windows.html

- gitlab runner windows installation

```gitlabrunner
 cd C:\

 mkdir GitLab-Runner

 cd GitLab-Runner

 curl -o gitlab-runner.exe -# -LO https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-windows-amd64.exe

 .\gitlab-runner.exe install

```

- windows delete a service

```delete
SC DELETE gitlab-runner
```

- gitlab runner registeration with Windows

- url https://docs.gitlab.com/runner/register/index.html

```register
.\gitlab-runner.exe start
.\gitlab-runner.exe register

shell

```

- shell https://stackoverflow.com/questions/68109273/exec-pwsh-executable-file-not-found-in-path

cmd

